package camel;


public class Data {
	 private String status;
	 private String id;


	 // Getter Methods 

	 public String getStatus() {
	  return status;
	 }

	 public String getId() {
	  return id;
	 }

	 // Setter Methods 

	 public void setStatus(String status) {
	  this.status = status;
	 }

	 public void setId(String id) {
	  this.id = id;
	 }
	}