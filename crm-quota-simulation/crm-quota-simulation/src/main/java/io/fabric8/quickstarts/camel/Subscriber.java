package io.fabric8.quickstarts.camel;

public class Subscriber {

	private String serialNumber;
	private String SubscriberId;
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getSubscriberId() {
		return SubscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		SubscriberId = subscriberId;
	}
	
	
}
